{-# OPTIONS_HADDOCK hide #-}

{-# LANGUAGE MagicHash #-}
{-# LANGUAGE UnboxedTuples #-}

module Swiss.Generation where

import Control.Monad.ST
import Control.Monad.Primitive
import qualified Data.Bits as B
import Data.Primitive
import GHC.Exts
import GHC.Word
import qualified Data.Vector.Primitive.Mutable as MPR


newtype SGen s = SGen (MutableByteArray s)

-- layout:
-- cur occupancy (64 bits) | mask (64 bits) | bucket count (64 bits) | dead occupancy (64 bits) | swiss buff (bucket count * 16 bits)

-- bucket count must be a power of 2 or the sum of two consecutive powers of 2 (e.g., 64 + 32 = 96)
newGen :: Int -> Int -> ST s (SGen s)
newGen bc@(I# bc') extra_slot_space = SGen <$> do  
  m@(MutableByteArray mba) <- do
    let sz = 4 * 8 + 2 * bc + extra_slot_space * bc
    newByteArray sz

  fillByteArray m 0 (32 + 2 * bc) 0

  let msk = if B.popCount bc == 1
               then case bc - 1 of
                      I# i -> intToInt64# i
               else do
                 let b_small = (2 :: Int) ^ B.countTrailingZeros bc
                     b_big = B.shiftL b_small 1
                 case b_big - 1 of
                   I# i -> intToInt64# i
  
  primitive \s -> (# writeInt64Array# mba 1# msk s , () #)
  primitive \s -> (# writeInt64Array# mba 2# (intToInt64# bc') s , () #)

  pure m

{-# INLINE bucketCount #-}
bucketCount :: SGen s -> ST s Int
bucketCount (SGen (MutableByteArray mba)) =
  primitive \s0 -> case readInt64Array# mba 2# s0 of
    (# s1 , i #) -> (# s1 , I# (int64ToInt# i) #)

{-# INLINE occupancy #-}
occupancy :: SGen s -> ST s Int
occupancy (SGen (MutableByteArray mba)) =
  primitive \s0 -> case readInt64Array# mba 0# s0 of
    (# s1 , i #) -> (# s1 , I# (int64ToInt# i) #)

{-# INLINE setOccupancy #-}
setOccupancy :: SGen s -> Int -> ST s ()
setOccupancy (SGen (MutableByteArray mba)) (I# c) =
  primitive \s -> (# writeInt64Array# mba 0# (intToInt64# c) s , () #)

{-# INLINE deadOccupancy #-}
deadOccupancy :: SGen s -> ST s Int
deadOccupancy (SGen (MutableByteArray mba)) =
  primitive \s0 -> case readInt64Array# mba 3# s0 of
    (# s1 , i #) -> (# s1 , I# (int64ToInt# i) #)

{-# INLINE setDeadOccupancy #-}
setDeadOccupancy :: SGen s -> Int -> ST s ()
setDeadOccupancy (SGen (MutableByteArray mba)) (I# c) =
  primitive \s -> (# writeInt64Array# mba 3# (intToInt64# c) s , () #)

{-# INLINE swissVec #-}
swissVec :: SGen s -> ST s (MPR.MVector s Word16)
swissVec s@(SGen mba) = do
  l <- bucketCount s
  pure (MPR.MVector 16 l mba)

