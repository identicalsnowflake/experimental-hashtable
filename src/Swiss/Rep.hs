{-# OPTIONS_HADDOCK hide #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE MagicHash #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeFamilyDependencies #-}

module Swiss.Rep where

import Control.Monad.ST
import Data.Kind (Type)
import Data.Primitive.ByteArray
import Data.Primitive.Types
import qualified Data.Vector.Generic as GV
import qualified Data.Vector.Generic.Mutable as MGV
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV
import qualified Data.Vector.Packed.Mutable as MPV
import qualified Data.Vector.Packed.Internal as MPV
import qualified Data.Vector.Primitive.Mutable as MPR
import qualified Data.Vector.Storable as SV
import qualified Data.Vector.Storable.Mutable as MSV
import qualified Data.Vector.Unboxed as UV
import qualified Data.Vector.Unboxed.Mutable as MUV
import GHC.Exts (reallyUnsafePtrEquality#)

import qualified Swiss.Generation as G


-- | A representation choice for keys or values in a hash table, denoting backing by the corresponding vector type.
data Rep = Boxed | Unboxed | Storable | Primitive | Packed

type family MVRep (rep :: Rep) = (mv :: Type -> Type -> Type) | mv -> rep where
  MVRep Boxed = MV.MVector
  MVRep Unboxed = MUV.MVector
  MVRep Storable = MSV.MVector
  MVRep Primitive = MPR.MVector
  MVRep Packed = MPV.MVector

data MTwoVec r1 r2 mkv mvv s k v = MTwoVec (mkv s k) (mvv s v) (G.SGen s)
data MKVec r1 r2 mkv s k v = MKVec (mkv s k) (G.SGen s)
data MVVec r1 r2 mvv s k v = MVVec (mvv s v) (G.SGen s)
newtype MBufOnly r1 r2 s k v = MBufOnly (G.SGen s)

data TwoVec r1 r2 mkv mvv k v = TwoVec (mkv k) (mvv v) ByteArray
data KVec r1 r2 mkv k v = KVec (mkv k) ByteArray
data VVec r1 r2 mvv k v = VVec (mvv v) ByteArray
newtype BufOnly r1 r2 k v = BufOnly ByteArray


-- the representation for a generation of a hash table
type family MGRep (r1 :: Rep) (r2 :: Rep) = (r :: Type -> Type -> Type -> Type) | r -> r1 r2 where
  MGRep Boxed Boxed = MTwoVec Boxed Boxed MV.MVector MV.MVector
  MGRep Boxed Unboxed = MTwoVec Boxed Unboxed MV.MVector MUV.MVector
  MGRep Boxed Storable = MTwoVec Boxed Storable MV.MVector MSV.MVector
  MGRep Boxed Primitive = MKVec Boxed Primitive MV.MVector
  MGRep Boxed Packed = MKVec Boxed Packed MV.MVector

  MGRep Unboxed Boxed = MTwoVec Unboxed Boxed MUV.MVector MV.MVector
  MGRep Unboxed Unboxed = MTwoVec Unboxed Unboxed MUV.MVector MUV.MVector
  MGRep Unboxed Storable = MTwoVec Unboxed Storable MUV.MVector MSV.MVector
  MGRep Unboxed Primitive = MKVec Unboxed Primitive MUV.MVector
  MGRep Unboxed Packed = MKVec Unboxed Packed MUV.MVector

  MGRep Storable Boxed = MTwoVec Storable Boxed MSV.MVector MV.MVector
  MGRep Storable Unboxed = MTwoVec Storable Unboxed MSV.MVector MUV.MVector
  MGRep Storable Storable = MTwoVec Storable Storable MSV.MVector MSV.MVector
  MGRep Storable Primitive = MKVec Storable Primitive MSV.MVector
  MGRep Storable Packed = MKVec Storable Packed MSV.MVector

  MGRep Primitive Boxed = MVVec Primitive Boxed MV.MVector
  MGRep Primitive Unboxed = MVVec Primitive Unboxed MUV.MVector
  MGRep Primitive Storable = MVVec Primitive Storable MSV.MVector
  MGRep Primitive Primitive = MBufOnly Primitive Primitive
  MGRep Primitive Packed = MBufOnly Primitive Packed

  MGRep Packed Boxed = MVVec Packed Boxed MV.MVector
  MGRep Packed Unboxed = MVVec Packed Unboxed MUV.MVector
  MGRep Packed Storable = MVVec Packed Storable MSV.MVector
  MGRep Packed Primitive = MBufOnly Packed Primitive
  MGRep Packed Packed = MBufOnly Packed Packed

type family GRep (r1 :: Rep) (r2 :: Rep) = (r :: Type -> Type -> Type) | r -> r1 r2 where
  GRep Boxed Boxed = TwoVec Boxed Boxed V.Vector V.Vector
  GRep Boxed Unboxed = TwoVec Boxed Unboxed V.Vector UV.Vector
  GRep Boxed Storable = TwoVec Boxed Storable V.Vector SV.Vector
  GRep Boxed Primitive = KVec Boxed Primitive V.Vector
  GRep Boxed Packed = KVec Boxed Packed V.Vector

  GRep Unboxed Boxed = TwoVec Unboxed Boxed UV.Vector V.Vector
  GRep Unboxed Unboxed = TwoVec Unboxed Unboxed UV.Vector UV.Vector
  GRep Unboxed Storable = TwoVec Unboxed Storable UV.Vector SV.Vector
  GRep Unboxed Primitive = KVec Unboxed Primitive UV.Vector
  GRep Unboxed Packed = KVec Unboxed Packed UV.Vector

  GRep Storable Boxed = TwoVec Storable Boxed SV.Vector V.Vector
  GRep Storable Unboxed = TwoVec Storable Unboxed SV.Vector UV.Vector
  GRep Storable Storable = TwoVec Storable Storable SV.Vector SV.Vector
  GRep Storable Primitive = KVec Storable Primitive SV.Vector
  GRep Storable Packed = KVec Storable Packed SV.Vector

  GRep Primitive Boxed = VVec Primitive Boxed V.Vector
  GRep Primitive Unboxed = VVec Primitive Unboxed UV.Vector
  GRep Primitive Storable = VVec Primitive Storable SV.Vector
  GRep Primitive Primitive = BufOnly Primitive Primitive
  GRep Primitive Packed = BufOnly Primitive Packed

  GRep Packed Boxed = VVec Packed Boxed V.Vector
  GRep Packed Unboxed = VVec Packed Unboxed UV.Vector
  GRep Packed Storable = VVec Packed Storable SV.Vector
  GRep Packed Primitive = BufOnly Packed Primitive
  GRep Packed Packed = BufOnly Packed Packed

class (MGV.MVector (MVRep rk) k , MGV.MVector (MVRep rv) v) => MSRep (rk :: Rep) (rv :: Rep) k v where

  {-# INLINE ptrCompare #-}
  ptrCompare :: k -> k -> Bool
  ptrCompare _ _ = False

  getMKV :: MGRep rk rv s k v -> ST s (MVRep rk s k)
  getMVV :: MGRep rk rv s k v -> ST s (MVRep rv s v)
  getMGenMeta :: MGRep rk rv s k v -> G.SGen s

  unsafeFreeze :: MGRep rk rv s k v -> ST s (GRep rk rv k v)
  unsafeThaw :: GRep rk rv k v -> ST s (MGRep rk rv s k v)
  freeze :: MGRep rk rv s k v -> ST s (GRep rk rv k v)
  
  -- input must be a power of 2 or the sum of two consecutive
  -- powers of 2 (e.g., 48 = 32 + 16)
  new :: Int -> ST s (MGRep rk rv s k v)
  

instance MSRep Boxed Boxed k v where
  {-# INLINE ptrCompare #-}
  ptrCompare x y = case reallyUnsafePtrEquality# x y of
    0# -> False
    _ -> True
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance MUV.Unbox v => MSRep Boxed Unboxed k v where
  {-# INLINE ptrCompare #-}
  ptrCompare x y = case reallyUnsafePtrEquality# x y of
    0# -> False
    _ -> True
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance MSV.Storable v => MSRep Boxed Storable k v where
  {-# INLINE ptrCompare #-}
  ptrCompare x y = case reallyUnsafePtrEquality# x y of
    0# -> False
    _ -> True
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance MPR.Prim v => MSRep Boxed Primitive k v where
  {-# INLINE ptrCompare #-}
  ptrCompare x y = case reallyUnsafePtrEquality# x y of
    0# -> False
    _ -> True
  {-# INLINE getMKV #-}
  getMKV (MKVec mkv _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MKVec _ g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: v)) bc mba)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MKVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MKVec mkv (G.SGen mba)) = do
    KVec <$> GV.unsafeFreeze mkv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (KVec kv ba) = do
    MKVec <$> GV.unsafeThaw kv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MKVec mkv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    KVec <$> GV.freeze mkv
         <*> freezeByteArray mba 0 (32 + bc * sizeOf (undefined :: v))

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    s <- G.newGen bc (sizeOf (undefined :: v))
    pure (MKVec mkv s)

instance MPV.Pack v => MSRep Boxed Packed k v where
  {-# INLINE ptrCompare #-}
  ptrCompare x y = case reallyUnsafePtrEquality# x y of
    0# -> False
    _ -> True
  {-# INLINE getMKV #-}
  getMKV (MKVec mkv _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MKVec _ sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MKVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MKVec mkv (G.SGen mba)) = do
    KVec <$> GV.unsafeFreeze mkv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (KVec kv ba) = do
    MKVec <$> GV.unsafeThaw kv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MKVec mkv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    KVec <$> GV.freeze mkv
         <*> freezeByteArray mba 0 (32 + bc * MPV.sizeInBytes @v)

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    s <- G.newGen bc (MPV.sizeInBytes @v)
    pure (MKVec mkv s)


instance MUV.Unbox k => MSRep Unboxed Boxed k v where
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance (MUV.Unbox k , MUV.Unbox v) => MSRep Unboxed Unboxed k v where
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance (MUV.Unbox k , MSV.Storable v) => MSRep Unboxed Storable k v where
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance (MUV.Unbox k , MPR.Prim v) => MSRep Unboxed Primitive k v where
  {-# INLINE getMKV #-}
  getMKV (MKVec mkv _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MKVec _ g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: v)) bc mba)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MKVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MKVec mkv (G.SGen mba)) = do
    KVec <$> GV.unsafeFreeze mkv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (KVec kv ba) = do
    MKVec <$> GV.unsafeThaw kv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MKVec mkv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    KVec <$> GV.freeze mkv
         <*> freezeByteArray mba 0 (32 + bc * sizeOf (undefined :: v))

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    s <- G.newGen bc (sizeOf (undefined :: v))
    pure (MKVec mkv s)

instance (MUV.Unbox k , MPV.Pack v) => MSRep Unboxed Packed k v where
  {-# INLINE getMKV #-}
  getMKV (MKVec mkv _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MKVec _ sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MKVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MKVec mkv (G.SGen mba)) = do
    KVec <$> GV.unsafeFreeze mkv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (KVec kv ba) = do
    MKVec <$> GV.unsafeThaw kv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MKVec mkv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    KVec <$> GV.freeze mkv
         <*> freezeByteArray mba 0 (32 + bc * MPV.sizeInBytes @v)

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    s <- G.newGen bc (MPV.sizeInBytes @v)
    pure (MKVec mkv s)


instance MSV.Storable k => MSRep Storable Boxed k v where
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance (MSV.Storable k , MUV.Unbox v) => MSRep Storable Unboxed k v where
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance (MSV.Storable k , MSV.Storable v) => MSRep Storable Storable k v where
  {-# INLINE getMKV #-}
  getMKV (MTwoVec mkv _ _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MTwoVec _ mvv _) = pure mvv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MTwoVec _ _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.unsafeFreeze mkv
           <*> GV.unsafeFreeze mvv
           <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (TwoVec kv mv ba) = do
    MTwoVec <$> GV.unsafeThaw kv
            <*> GV.unsafeThaw mv
            <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MTwoVec mkv mvv (G.SGen mba)) = do
    TwoVec <$> GV.freeze mkv
           <*> GV.freeze mvv
           <*> freezeByteArray mba 0 32

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc 0
    pure (MTwoVec mkv mvv s)

instance (MSV.Storable k , MPR.Prim v) => MSRep Storable Primitive k v where
  {-# INLINE getMKV #-}
  getMKV (MKVec mkv _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MKVec _ g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: v)) bc mba)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MKVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MKVec mkv (G.SGen mba)) = do
    KVec <$> GV.unsafeFreeze mkv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (KVec kv ba) = do
    MKVec <$> GV.unsafeThaw kv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MKVec mkv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    KVec <$> GV.freeze mkv
         <*> freezeByteArray mba 0 (32 + bc * sizeOf (undefined :: v))

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    s <- G.newGen bc (sizeOf (undefined :: v))
    pure (MKVec mkv s)

instance (MSV.Storable k , MPV.Pack v) => MSRep Storable Packed k v where
  {-# INLINE getMKV #-}
  getMKV (MKVec mkv _) = pure mkv
  {-# INLINE getMVV #-}
  getMVV (MKVec _ sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MKVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MKVec mkv (G.SGen mba)) = do
    KVec <$> GV.unsafeFreeze mkv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (KVec kv ba) = do
    MKVec <$> GV.unsafeThaw kv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MKVec mkv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    KVec <$> GV.freeze mkv
         <*> freezeByteArray mba 0 (32 + bc * MPV.sizeInBytes @v)

  {-# INLINE new #-}
  new bc = do
    mkv <- MGV.unsafeNew bc
    s <- G.newGen bc (MPV.sizeInBytes @v)
    pure (MKVec mkv s)


instance MPR.Prim k => MSRep Primitive Boxed k v where
  {-# INLINE getMKV #-}
  getMKV (MVVec _ g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: k)) bc mba)
  {-# INLINE getMVV #-}
  getMVV (MVVec vv _) = pure vv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MVVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MVVec mvv (G.SGen mba)) = do
    VVec <$> GV.unsafeFreeze mvv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (VVec vv ba) = do
    MVVec <$> GV.unsafeThaw vv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MVVec mvv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    VVec <$> GV.freeze mvv
         <*> freezeByteArray mba 0 (32 + bc * sizeOf (undefined :: k))

  {-# INLINE new #-}
  new bc = do
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc (sizeOf (undefined :: k))
    pure (MVVec mvv s)

instance (MPR.Prim k , MUV.Unbox v) => MSRep Primitive Unboxed k v where
  {-# INLINE getMKV #-}
  getMKV (MVVec _ g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: k)) bc mba)
  {-# INLINE getMVV #-}
  getMVV (MVVec vv _) = pure vv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MVVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MVVec mvv (G.SGen mba)) = do
    VVec <$> GV.unsafeFreeze mvv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (VVec vv ba) = do
    MVVec <$> GV.unsafeThaw vv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MVVec mvv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    VVec <$> GV.freeze mvv
         <*> freezeByteArray mba 0 (32 + bc * sizeOf (undefined :: k))

  {-# INLINE new #-}
  new bc = do
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc (sizeOf (undefined :: k))
    pure (MVVec mvv s)

instance (MPR.Prim k , MSV.Storable v) => MSRep Primitive Storable k v where
  {-# INLINE getMKV #-}
  getMKV (MVVec _ g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: k)) bc mba)
  {-# INLINE getMVV #-}
  getMVV (MVVec vv _) = pure vv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MVVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MVVec mvv (G.SGen mba)) = do
    VVec <$> GV.unsafeFreeze mvv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (VVec vv ba) = do
    MVVec <$> GV.unsafeThaw vv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MVVec mvv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    VVec <$> GV.freeze mvv
         <*> freezeByteArray mba 0 (32 + bc * sizeOf (undefined :: k))

  {-# INLINE new #-}
  new bc = do
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc (sizeOf (undefined :: k))
    pure (MVVec mvv s)

instance (MPR.Prim k , MPR.Prim v) => MSRep Primitive Primitive k v where
  {-# INLINE getMKV #-}
  getMKV (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: k)) bc mba)
  {-# INLINE getMVV #-}
  getMVV (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: v) + bc * sizeOf (undefined :: k)) bc mba)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MBufOnly g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MBufOnly (G.SGen mba)) = do
    BufOnly <$> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (BufOnly ba) = do
    MBufOnly <$> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    BufOnly <$> freezeByteArray mba 0 (32 + bc * (sizeOf (undefined :: k) + sizeOf (undefined :: v)))

  {-# INLINE new #-}
  new bc = MBufOnly <$> G.newGen bc (sizeOf (undefined :: k) + sizeOf (undefined :: v))

instance (MPR.Prim k , MPV.Pack v) => MSRep Primitive Packed k v where
  {-# INLINE getMKV #-}
  getMKV (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: k)) bc mba)
  {-# INLINE getMVV #-}
  getMVV (MBufOnly sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + (2 + sizeOf (undefined :: k)) * bc) bc)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MBufOnly g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MBufOnly (G.SGen mba)) = do
    BufOnly <$> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (BufOnly ba) = do
    MBufOnly <$> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    BufOnly <$> freezeByteArray mba 0 (32 + bc * (sizeOf (undefined :: k) + MPV.sizeInBytes @v))

  {-# INLINE new #-}
  new bc = MBufOnly <$> G.newGen bc (sizeOf (undefined :: k) + MPV.sizeInBytes @v)


instance MPV.Pack k => MSRep Packed Boxed k v where
  {-# INLINE getMKV #-}
  getMKV (MVVec _ sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMVV #-}
  getMVV (MVVec vv _) = pure vv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MVVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MVVec mvv (G.SGen mba)) = do
    VVec <$> GV.unsafeFreeze mvv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (VVec vv ba) = do
    MVVec <$> GV.unsafeThaw vv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MVVec mvv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    VVec <$> GV.freeze mvv
         <*> freezeByteArray mba 0 (32 + bc * MPV.sizeInBytes @k)

  {-# INLINE new #-}
  new bc = do
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc (MPV.sizeInBytes @k)
    pure (MVVec mvv s)

instance (MPV.Pack k , MUV.Unbox v) => MSRep Packed Unboxed k v where
  {-# INLINE getMKV #-}
  getMKV (MVVec _ sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMVV #-}
  getMVV (MVVec vv _) = pure vv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MVVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MVVec mvv (G.SGen mba)) = do
    VVec <$> GV.unsafeFreeze mvv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (VVec vv ba) = do
    MVVec <$> GV.unsafeThaw vv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MVVec mvv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    VVec <$> GV.freeze mvv
         <*> freezeByteArray mba 0 (32 + bc * MPV.sizeInBytes @k)

  {-# INLINE new #-}
  new bc = do
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc (MPV.sizeInBytes @k)
    pure (MVVec mvv s)

instance (MPV.Pack k , MSV.Storable v) => MSRep Packed Storable k v where
  {-# INLINE getMKV #-}
  getMKV (MVVec _ sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMVV #-}
  getMVV (MVVec vv _) = pure vv
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MVVec _ g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MVVec mvv (G.SGen mba)) = do
    VVec <$> GV.unsafeFreeze mvv
         <*> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (VVec vv ba) = do
    MVVec <$> GV.unsafeThaw vv
          <*> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MVVec mvv g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    VVec <$> GV.freeze mvv
         <*> freezeByteArray mba 0 (32 + bc * MPV.sizeInBytes @k)

  {-# INLINE new #-}
  new bc = do
    mvv <- MGV.unsafeNew bc
    s <- G.newGen bc (MPV.sizeInBytes @k)
    pure (MVVec mvv s)

instance (MPV.Pack k , MPR.Prim v) => MSRep Packed Primitive k v where
  {-# INLINE getMKV #-}
  getMKV (MBufOnly sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMVV #-}
  getMVV (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    pure (MPR.MVector ((32 + 2 * bc) `div` sizeOf (undefined :: v) + bc * MPV.sizeInBytes @k) bc mba)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MBufOnly g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MBufOnly (G.SGen mba)) = do
    BufOnly <$> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (BufOnly ba) = do
    MBufOnly <$> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    BufOnly <$> freezeByteArray mba 0 (32 + bc * (MPV.sizeInBytes @k + sizeOf (undefined :: v)))

  {-# INLINE new #-}
  new bc = MBufOnly <$> G.newGen bc (MPV.sizeInBytes @k + sizeOf (undefined :: v))

instance (MPV.Pack k , MPV.Pack v) => MSRep Packed Packed k v where
  {-# INLINE getMKV #-}
  getMKV (MBufOnly sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + 2 * bc) bc)
  {-# INLINE getMVV #-}
  getMVV (MBufOnly sg@(G.SGen (MutableByteArray mba))) = do
    bc <- G.bucketCount sg
    pure (MPV.MVector mba (32 + (2 + MPV.sizeInBytes @k) * bc) bc)
  {-# INLINE getMGenMeta #-}
  getMGenMeta (MBufOnly g) = g

  {-# INLINE unsafeFreeze #-}
  unsafeFreeze (MBufOnly (G.SGen mba)) = do
    BufOnly <$> unsafeFreezeByteArray mba
  {-# INLINE unsafeThaw #-}
  unsafeThaw (BufOnly ba) = do
    MBufOnly <$> (G.SGen <$> unsafeThawByteArray ba)
  {-# INLINE freeze #-}
  freeze (MBufOnly g@(G.SGen mba)) = do
    bc <- G.bucketCount g
    BufOnly <$> freezeByteArray mba 0 (32 + bc * (MPV.sizeInBytes @k + MPV.sizeInBytes @v))

  {-# INLINE new #-}
  new bc = MBufOnly <$> G.newGen bc (MPV.sizeInBytes @k + MPV.sizeInBytes @v)

