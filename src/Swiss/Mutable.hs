-- | A hash table implementation, based on bulk 16-bit linear probing.

module Swiss.Mutable
  ( MSwiss
  , Swiss.Mutable.new
  , insert
  , Swiss.Mutable.lookup
  , lookupKnown
  , delete
  , exists
  , occupancy
  , bucketCount
  ) where

import Control.Monad.Primitive
import Control.Monad.ST
import qualified Data.Bits as B
import Data.Hashable
import Data.STRef
import qualified Data.Vector.Generic.Mutable as MGV

import qualified Swiss.CBindings as C
import qualified Swiss.Generation as G
import Swiss.Mutable.Internal
import Swiss.Rep


{-# INLINE new #-}
new :: (MSRep rk rv k v , PrimMonad m) => Int -> m (MSwiss rk rv (PrimState m) k v)
new i = unsafeSTToPrim (new' i)

{-# INLINABLE new' #-}
new' :: MSRep rk rv k v => Int -> ST s (MSwiss rk rv s k v)
new' min_capacity = do
  let !bc = fittingBucketCount min_capacity
  g :: MGRep rk rv s k v <- Swiss.Rep.new bc
  MSwiss <$> newSTRef g

{-# INLINE insert #-}
-- | Insert the given key/value pair into the hash table. If this key already exists, the old value will be discarded and replaced with the given value.
insert :: (Hashable k , MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> k -> v -> m ()
insert s k v = unsafeSTToPrim (insert' s k v)

{-# INLINABLE insert' #-}
insert' :: (Hashable k , MSRep rk rv k v) => MSwiss rk rv s k v -> k -> v -> ST s ()
insert' mi@(MSwiss r) k v = do
  let !h = hashInt (hash k)
  gr :: MGRep rk rv s k v <- readSTRef r
  let g = getMGenMeta gr
  mk <- getMKV gr
  mv <- getMVV gr
  i <- find_slot gr k h
  sv <- G.swissVec g
  w <- MGV.unsafeRead sv i
  if w == 0
     then do
       cur_occ <- G.occupancy g
       bc <- G.bucketCount g
       G.setOccupancy g (cur_occ + 1)
       MGV.unsafeWrite sv i (32768 B..|. fromIntegral h)
       MGV.unsafeWrite mk i k
       MGV.unsafeWrite mv i v       
       if 100 * cur_occ <= 85 * bc
          then pure ()
          else resize mi
     else MGV.unsafeWrite mv i v

{-# INLINEABLE resize #-}
resize :: (Hashable k ,  MSRep rk rv k v) => MSwiss rk rv s k v -> ST s ()
resize (MSwiss r) = do
  gr <- readSTRef r
  let g = getMGenMeta gr
  mk <- getMKV gr
  mv <- getMVV gr
  cur_occ <- G.occupancy g
  bc <- G.bucketCount g
  
  tombs <- G.deadOccupancy g
  let live = cur_occ - tombs
  gr' :: MGRep rk rv s k v <- do
    Swiss.Rep.new
      if 2 * live < bc
         then fittingBucketCount live
         else if B.popCount bc == 1
                 then bc + (B.shiftR bc 1)
                 else 2 ^ (2 + B.countTrailingZeros bc)
  let g' = getMGenMeta gr'
  osv <- G.swissVec g
  nsv <- G.swissVec g'
  writeSTRef r gr'
  nmk <- getMKV gr'
  nmv <- getMVV gr'
  G.setOccupancy g' live
  G.setDeadOccupancy g' 0

  MGV.iforM_ osv \i_ w -> do
    -- use >= here instead of checking for zero, as this properly skips tombstones
    if w >= 32768
       then do
         k_ <- MGV.unsafeRead mk i_
         v_ <- MGV.unsafeRead mv i_
         C.find_empty g' (hashInt (hash k_)) >>= \j -> do
           MGV.unsafeWrite nsv j w
           MGV.unsafeWrite nmk j k_
           MGV.unsafeWrite nmv j v_
       else pure ()

{-# INLINE lookup #-}
-- | Lookup the given key in the hash table. If it is not present, @Nothing@ is returned. If you know this key to be in the table, you may use 'lookupKnown', which is more efficient.
lookup :: (Hashable k , MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> k -> m (Maybe v)
lookup (MSwiss r) k = unsafeSTToPrim do
  gr <- readSTRef r
  sv <- G.swissVec (getMGenMeta gr)
  i <- find_slot gr k (hashInt (hash k))
  MGV.unsafeRead sv i >>= \case
    0 -> pure Nothing
    _ -> do
      mv <- getMVV gr
      v <- MGV.unsafeRead mv i
      pure (Just v)

{-# INLINE alter #-}
-- | Performs a lookup, evaluates the given function on the result, and stores the result back in the table. If 'Nothing' is returned, the key is deleted from the table. 'alter' is more efficient than performing the combination of operations yourself using 'lookup' and 'insert'.
alter :: (Hashable k , MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> k -> (Maybe v -> Maybe v) -> m ()
alter (MSwiss r) k f = unsafeSTToPrim do
  gr <- readSTRef r
  sv <- G.swissVec (getMGenMeta gr)
  mv <- getMVV gr
  i <- find_slot gr k (hashInt (hash k))
  MGV.unsafeRead sv i >>= \case
    0 -> case f Nothing of
      Nothing -> pure ()
      Just v -> do
        -- TODO: inline the @ins@ logic, as we're looking up the index again with
        -- this call to insert'.
        insert' (MSwiss r) k v
    _ -> do
      v <- MGV.unsafeRead mv i
      case f (Just v) of
        Nothing -> MGV.unsafeWrite sv i 1
        Just v' -> MGV.unsafeWrite mv i v'


{-# INLINE exists #-}
exists :: (Hashable k , MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> k -> m Bool
exists (MSwiss r) k = unsafeSTToPrim do
  gr <- readSTRef r
  sv <- G.swissVec (getMGenMeta gr)
  i <- find_slot gr k (hashInt (hash k))
  (0/=) <$> MGV.unsafeRead sv i

{-# INLINE lookupKnown #-}
-- | The given key must be in the table, or the behavior is undefined. Faster than 'lookup'.
lookupKnown :: (Hashable k , MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> k -> m v
lookupKnown (MSwiss r) k = unsafeSTToPrim do
  gr <- readSTRef r
  mv <- getMVV gr
  find_known_slot gr k (hashInt (hash k)) >>= MGV.unsafeRead mv


{-# INLINE delete #-}
delete :: (Hashable k , MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> k -> m Bool
delete m k = unsafeSTToPrim (delete' m k)

{-# INLINABLE delete' #-}
delete' :: (Hashable k , MSRep rk rv k v) => MSwiss rk rv s k v -> k -> ST s Bool
delete' (MSwiss r) k = do
  gr <- readSTRef r
  let g = getMGenMeta gr
      h = hashInt (hash k)
  i <- find_slot gr k h
  sv <- G.swissVec g
  mt <- MGV.unsafeRead sv i
  if mt == 0
     then pure False
     else do
       G.deadOccupancy g >>= G.setDeadOccupancy g . (1+)
       MGV.unsafeWrite sv i 1
       pure True

