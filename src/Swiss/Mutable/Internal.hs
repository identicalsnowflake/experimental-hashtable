{-# OPTIONS_HADDOCK hide #-}

{-# LANGUAGE MagicHash #-}
{-# LANGUAGE TypeFamilyDependencies #-}

module Swiss.Mutable.Internal
  ( Swiss(..)
  , MSwiss(..)
  , fittingBucketCount
  , occupancy
  , bucketCount
  , forEachLiveKeyValue
  , set_slot
  , find_slot
  , find_known_slot
  , slow_find_slot
  , hashInt
  ) where

import Control.Monad.Primitive
import Control.Monad.ST
import qualified Data.Bits as B
import Data.STRef
import qualified Data.Vector.Generic.Mutable as MGV
import GHC.Exts
import GHC.Int
import GHC.Integer.Logarithms

import qualified Swiss.CBindings as C
import qualified Swiss.Generation as G
import Swiss.Rep


-- | An immutable hash table, backed by the chosen key and value representations
newtype Swiss rk rv k v = Swiss (GRep rk rv k v)

-- | A mutable hash table, backed by the chosen key and value representations
newtype MSwiss rk rv s k v = MSwiss (STRef s (MGRep rk rv s k v))

{-# NOINLINE fittingBucketCount #-}
-- ideal capacity is 50-70%. fuller capacities see performance drop off, lesser capacities waste space
fittingBucketCount :: Int -> Int
fittingBucketCount i@(I# i') = do
  let !sbc = 2 ^ do 1 + I# (wordLog2# (int2Word# i'))
  max 32 $ if 100 * i <= 83 * sbc
    then sbc
    else do
      let halfway = B.shiftR sbc 1 + sbc
      if 100 * i <= 83 * halfway
         then halfway
         else 2 * sbc

{-# INLINE occupancy #-}
-- | Includes tombstones
occupancy :: (MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> m Int
occupancy (MSwiss r) = unsafeSTToPrim do readSTRef r >>= G.occupancy . getMGenMeta

{-# INLINE bucketCount #-}
-- | Number of buckets in the current generation of the table. Will always be either a power of two or the sum of two consecutive powers of two.
bucketCount :: (MSRep rk rv k v , PrimMonad m) => MSwiss rk rv (PrimState m) k v -> m Int
bucketCount (MSwiss r) = unsafeSTToPrim do readSTRef r >>= G.bucketCount . getMGenMeta

{-# INLINE forEachLiveKeyValue #-}
forEachLiveKeyValue :: MSRep rk rv k v => MGRep rk rv s k v -> (Int -> k -> v -> ST s ()) -> ST s ()
forEachLiveKeyValue gr f = do
  osv <- G.swissVec (getMGenMeta gr)
  mk <- getMKV gr
  mv <- getMVV gr
  MGV.iforM_ osv \i w -> do
    -- use >= here instead of checking for zero, as this properly skips tombstones
    if w >= 32768
       then do
         k <- MGV.unsafeRead mk i
         v <- MGV.unsafeRead mv i
         f i k v
       else pure ()

{-# INLINE set_slot #-}
set_slot :: MSRep rk rv k v => MGRep rk rv s k v -> Int -> v -> ST s ()
set_slot s i v = do
  mv <- getMVV s
  MGV.unsafeWrite mv i v

{-# INLINE find_slot #-}
find_slot :: (Eq k , MSRep rk rv k v) => MGRep rk rv s k v -> k -> Int64 -> ST s Int
find_slot s k h = do
  C.find_slot (getMGenMeta s) h >>= \raw_i -> if raw_i > 0
    then pure (raw_i - 1)
    else slow_find_slot s k (negate (raw_i + 1))

{-# INLINE find_known_slot #-}
-- given key must be in the table or the behavior is undefined
find_known_slot :: (Eq k , MSRep rk rv k v) => MGRep rk rv s k v -> k -> Int64 -> ST s Int
find_known_slot s k h = do
  C.find_known_slot (getMGenMeta s) h >>= \raw_i -> if raw_i > 0
    then pure (raw_i - 1)
    else slow_find_slot s k (negate (raw_i + 1))

{-# INLINABLE slow_find_slot #-}
-- could probably speed this up a bit, but benching has this slow path only
-- being taken ~1/1500th of the time
slow_find_slot :: forall rk rv s k v . (MSRep rk rv k v , Eq k)
               => MGRep rk rv s k v -> k -> Int -> ST s Int
slow_find_slot gr k = \i -> do
  mk <- getMKV gr
  k_ <- MGV.unsafeRead mk i
  if ptrCompare @rk @rv @k @v k k_
     then pure i
     else if k == k_
             then pure i
             else do
               let g = getMGenMeta gr
               sv <- G.swissVec g
               go sv mk if i == MGV.length mk then 0 else i
  where
    go sv mk = go'
      where
        go' i = do
          w <- MGV.unsafeRead sv i
          if w == 0
             then pure i
             else if w >= 32768
               then do
                 k_ <- MGV.unsafeRead mk i
                 if ptrCompare @rk @rv @k @v k k_
                    then pure i
                    else if k == k_
                            then pure i
                            else go' (if i == MGV.length mk - 1 then 0 else i + 1)
               else go' (if i == MGV.length mk - 1 then 0 else i + 1)

-- splitmix-ish, to ensure decent bit distribution even if they use a poor-quality hash
{-# INLINE hashInt #-}
hashInt :: Int -> Int64
hashInt (fromIntegral -> x) = do
  let a = (x `B.xor` (B.shiftR x 30)) * (-4658895280553007687)
      b = (a `B.xor` (B.shiftR a 27)) * (-7723592293110705685)
  b `B.xor` (B.shiftR b 31)

