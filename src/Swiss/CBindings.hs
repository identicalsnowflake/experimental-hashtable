{-# OPTIONS_HADDOCK hide #-}

{-# LANGUAGE MagicHash #-}
{-# LANGUAGE UnboxedTuples #-}
{-# LANGUAGE UnliftedFFITypes #-}

module Swiss.CBindings
  ( find_slot
  , find_empty
  , find_known_slot

  , dumpc
  ) where

import Control.Monad.ST
import Control.Monad.ST.Unsafe
import Data.Primitive
import GHC.Int

import Swiss.Generation

import Foreign.C.Types (CInt(..))

-- mini
foreign import ccall unsafe "static hprims.h find_slot" c_find_slot :: MutableByteArray# s -> Int64 -> IO Int
foreign import ccall unsafe "static hprims.h find_empty" c_find_empty :: MutableByteArray# s -> Int64 -> IO Int
foreign import ccall unsafe "static hprims.h find_known_slot" c_find_known_slot :: MutableByteArray# s -> Int64 -> IO Int
foreign import ccall unsafe "static hprims.h dumpc" c_dumpc :: CInt -> IO ()


{-# INLINE find_slot #-}
find_slot :: SGen s -> Int64 -> ST s Int
find_slot (SGen (MutableByteArray p)) h = unsafeIOToST (c_find_slot p h)

{-# INLINE find_empty #-}
find_empty :: SGen s -> Int64 -> ST s Int
find_empty (SGen (MutableByteArray p)) h = unsafeIOToST (c_find_empty p h)

{-# INLINE find_known_slot #-}
find_known_slot :: SGen s -> Int64 -> ST s Int
find_known_slot (SGen (MutableByteArray p)) h = unsafeIOToST (c_find_known_slot p h)

dumpc :: IO ()
dumpc = c_dumpc 0

