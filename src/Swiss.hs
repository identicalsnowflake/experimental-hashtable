module Swiss
  ( Swiss
  , fromFoldable
  , fromUniqueFoldable
  , (!)
  ) where

import Control.Monad.ST
import qualified Data.Bits as B
import Data.Foldable
import Data.Hashable
import qualified Data.Vector.Generic.Mutable as MGV

import qualified Swiss.CBindings as C
import qualified Swiss.Generation as G
import Swiss.Mutable.Internal
import Swiss.Rep


{-# INLINE fromFoldable #-}
fromFoldable :: (MSRep rk rv k v , Foldable f , Hashable k) => f (k,v) -> Swiss rk rv k v
fromFoldable xs = runST do
  let l = length xs
  m <- new (fittingBucketCount l)
  let !sg = getMGenMeta m
  metav <- G.swissVec sg
  kv <- getMKV m
  vv <- getMVV m
  G.setOccupancy sg l
  for_ xs \(k,v) -> do
    let h = hashInt (hash k)
    C.find_slot sg h >>= \raw_i -> if raw_i > 0
      then do
        let i = raw_i - 1
        MGV.unsafeWrite metav i (32768 B..|. fromIntegral h)
        MGV.unsafeWrite kv i k
        MGV.unsafeWrite vv i v
      else do
        i <- slow_find_slot m k (negate (raw_i + 1))
        MGV.unsafeRead metav i >>= \case
          0 -> do
            MGV.unsafeWrite metav i (32768 B..|. fromIntegral h)
            MGV.unsafeWrite kv i k
            MGV.unsafeWrite vv i v
          _ -> MGV.unsafeWrite vv i v
  Swiss <$> unsafeFreeze m


-- THIS IS THE FUNCTION IN QUESTION --
-- | Requires no duplicate keys be given, otherwise the behavior is undefined. More efficient than 'fromFoldable'.
fromUniqueFoldable :: (MSRep rk rv k v , Foldable f , Hashable k) => f (k,v) -> Swiss rk rv k v
fromUniqueFoldable xs = runST do
  let l = length xs
  m <- new (fittingBucketCount l)
  let !sg = getMGenMeta m
  metav <- G.swissVec sg
  kv <- getMKV m
  vv <- getMVV m
  G.setOccupancy sg l
  for_ xs \(k,v) -> do
    let h = hashInt (hash k)
    C.find_empty sg h >>= \i -> do
      MGV.unsafeWrite metav i (32768 B..|. fromIntegral h)
      MGV.unsafeWrite kv i k
      MGV.unsafeWrite vv i v
  Swiss <$> unsafeFreeze m

infixl 9 !
{-# INLINE (!) #-}
(!) :: (MSRep rk rv k v , Hashable k) => Swiss rk rv k v -> k -> v
(!) (Swiss s) k = runST do
  gr <- unsafeThaw s
  mv <- getMVV gr
  find_known_slot gr k (hashInt (hash k)) >>= MGV.unsafeRead mv

