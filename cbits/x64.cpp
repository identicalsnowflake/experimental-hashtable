#include "hprims.h"
#include <bit>
#include <bitset>
#include "version2/vectorclass.h"


// min size: 32 (one AVX2 chunk)
struct Swiss {
  size_t occupancy; // the number of elements currently in the table
  size_t mask;
  size_t bucket_count;  // min 32. must be either a power of 2 or 1.5 * a power of 2 (e.g., 32, 48, 64, 96)
  // size_t slot_size;
  uint16_t* swiss; // least-significant 15-bits act as hash
};

static uint64_t c1 = 0;
static uint64_t c2 = 0;

static inline Swiss get_swiss(void* sb) {
  uint64_t* b = (uint64_t*)sb;
  Swiss s;
  // s.occupancy = b[0];
  s.mask = b[1];
  s.bucket_count = b[2];
  // s.slot_size = b[3];
  s.swiss = (uint16_t*)((uint8_t*)sb + 32);
  return s;
}

static constexpr const int stride = 16;

static inline size_t h_ind(Swiss s , uint64_t h) {
  // hash h bit interpretation: 5 uninteresting bits | 39 bit index via masking | 5 bits for choosing a 1/3 random variable when needed | 15 bits swiss meta
  size_t hmasked = (h >> (59 - std::popcount(s.mask))) & s.mask;
  // when we're an off-power of 2, generate 3 uniform possibilities for prefixes: 10, 01, 00
  size_t i_pre = (std::popcount(s.bucket_count) == 2) & (((h >> 15) & 31) >= 21)
                 ? (2 << (std::countr_zero(s.mask + 1) - 1)) | (hmasked >> 1)
                 : hmasked
                 ;
  return std::min(s.bucket_count - stride , i_pre);
}

// if the index is known to be good, it is returned positive; if it is merely
// a potential candidate, then it is returned negative.
//
// in either case, the magnitude of the index is bumped by 1, since there is no
// way to distinguish between positive and negative 0
extern "C" size_t find_slot(void* sb, uint64_t h) {

  Swiss s = get_swiss(sb);

  size_t i = h_ind(s,h);

  while(true) {

    Vec16us v;
    v.load(s.swiss + i);

    Vec16sb zeros = v == 0;
    Vec16sb pot_matches = v == (32768 | h);

    constexpr const uint16_t m = stride == 16 ? 0xFFFF : 0x00FF;
    
    uint16_t zs = to_bits(zeros) & m;
    uint16_t interestings = to_bits(zeros | pot_matches) & m;

    if(zs != 0) {
      int first_z = std::countr_zero(zs);
      return std::countr_zero(interestings) == first_z ? 1 + i + first_z : -1 * (1 + i + std::countr_zero(interestings));
    }else if(interestings != 0) {
      return -1 * (1 + i + std::countr_zero(interestings));
    }
    
    i = i == (s.bucket_count - stride) ? 0 : std::min(s.bucket_count - stride,i + stride);
  }
  
}

// resize assist primitive. the returned index is intact, as it is
// always valid.
extern "C" size_t find_empty(void* sb , uint64_t h) {
  Swiss s = get_swiss(sb);

  size_t i = h_ind(s,h);

  while(true) {

    Vec16us v;
    v.load(s.swiss + i);

    Vec16sb zeros = v == 0;

    constexpr const uint16_t m = stride == 16 ? 0xFFFF : 0x00FF;
    
    uint16_t zs = to_bits(zeros) & m;

    if(zs != 0) {
      int first_z = std::countr_zero(zs);
      return i + first_z;

    }
    
    i = i == (s.bucket_count - stride) ? 0 : std::min(s.bucket_count - stride,i + stride);
  }
  
}

// the given hash must exist live within the table, otherwise the behavior is undefined.
// as with find_slot, the returned index magnitude is offset by 1, and is negative
// if the result is not known to be good, and positive if the result is known to be good.
extern "C" size_t find_known_slot(void* sb , uint64_t h) {
  Swiss s = get_swiss(sb);

  size_t i = h_ind(s,h);

  while(true) {

    Vec16us v;
    v.load(s.swiss + i);

    Vec16sb zeros = v == 0;
    Vec16sb pot_matches = v == (32768 | h);

    constexpr const uint16_t m = stride == 16 ? 0xFFFF : 0x00FF;
    
    uint16_t zs = to_bits(zeros) & m;
    uint16_t interestings = to_bits(zeros | pot_matches) & m;

    if(zs != 0) {
      int first_z = std::countr_zero(zs);
      interestings = interestings & (0xFFFF >> (16 - first_z));
      return std::popcount(interestings) == 1 ? 1 + i + std::countr_zero(interestings) : -1 * (1 + i + std::countr_zero(interestings));
    }else if(interestings != 0) {
      size_t found = 1 + i + std::countr_zero(interestings);
      if(std::popcount(interestings) != 1) {
        return -1 * found;
      }
      while(true){
        i = i == (s.bucket_count - stride) ? 0 : std::min(s.bucket_count - stride,i + stride);
        v.load(s.swiss + i);
  	zeros = v == 0;
	pot_matches = v == (32768 | h);
	zs = to_bits(zeros) & m;
	interestings = to_bits(pot_matches) & m;
	if(zs != 0) {
          int first_z = std::countr_zero(zs);
          interestings = interestings & (0xFFFF >> (16 - first_z));
          return std::popcount(interestings) == 0 ? found : -1 * found;
	}else{
          if(interestings != 0) {
            return -1 * found;
	  }
	}
      }
    }
    
    i = i == (s.bucket_count - stride) ? 0 : std::min(s.bucket_count - stride,i + stride);
  }
}

extern "C" void dumpc(int i) {
  printf("c1: %lu\n",c1);
  printf("c2: %lu\n",c2);
  fflush(stdout);
}

