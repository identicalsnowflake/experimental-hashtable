#ifndef HPRIMS_H
#define HPRIMS_H

#include <stddef.h>
#include <stdint.h>

extern "C" size_t find_slot(void* s, uint64_t h);
extern "C" size_t find_empty(void* s, uint64_t h);
extern "C" size_t find_known_slot(void* s, uint64_t h);

extern "C" void dumpc(int i);

#endif
