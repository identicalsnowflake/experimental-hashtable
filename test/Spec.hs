module Main where

import Data.Foldable
import qualified Data.Time.Clock as T
import qualified Data.Time.Clock.System as T
import GHC.Int
import GHC.Word

import Swiss
import Swiss.Rep (Rep(..))


{-# INLINE forish #-}
forish :: Int -> (Int -> IO ()) -> IO ()
forish i f = for_ [ 0 .. i ] f

data MyList a = MyList [ a ] Int
  deriving Functor

instance Foldable MyList where
  {-# INLINE foldr #-}
  foldr f z (MyList xs _) = foldr f z xs
  {-# INLINE length #-}
  length (MyList _ l) = l

main :: IO ()
main = do 
  let c :: Int
      c = 3000000

      xs :: [(Int32,Word8)]
      xs = ((\x -> (fromIntegral x,fromIntegral x)) <$> [ 0 .. c ])
  
  start <- T.getSystemTime

  let !m = fromUniqueFoldable @Primitive @Primitive (MyList xs (c + 1))
  
  end1 <- T.getSystemTime
  
  forish c \i -> do
    if fromIntegral i == (m ! fromIntegral i)
       then pure ()
       else error "boom"

  end2 <- T.getSystemTime

  let f s e = T.nominalDiffTimeToSeconds $ T.diffUTCTime (T.systemToUTCTime e) (T.systemToUTCTime s)

  putStrLn $ "total time:" <> show (f start end2)
  putStrLn $ "ins/sec: " <> show ((fromIntegral c / 1000000) / (f start end1))
  putStrLn $ "read/sec: " <> show ((fromIntegral c / 1000000) / (f end1 end2))  

